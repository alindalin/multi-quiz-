<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'multiQuiz' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'v&v:/f2>OMj.g9sK:e`m4mbQ#3V39Y<Ef:O#Bz<WdTukh*X9-3NHO5%#&XJT`PeN' );
define( 'SECURE_AUTH_KEY',  'g6c/:Vyjw{RSB!>&5[#PUX~@,c1y3}2MMG<G-ODtRL(y 1ep-a#G:5U&S||N}B^q' );
define( 'LOGGED_IN_KEY',    'T$li-2mTV`vqm:f^bLv0,;?z}r9%b3@f(n .Cn3zJ6.EJI@pE-$J V}Get-,SN[Q' );
define( 'NONCE_KEY',        'n$sAFH<>e+0,A.EQm_z!/{K;]2iyya)dnO}TgJy,4OM=x?D3B17^^eeC5A$2=XE,' );
define( 'AUTH_SALT',        'kH$OQ}^n]#bm`U,}WVWgT9[!UY$**^AS@j4xT]|=x0ZB:y.2iY#Z{[0U_xO3e.u]' );
define( 'SECURE_AUTH_SALT', '1jByYQc22H>)<zj2Hig08Jm?t-V]e!<~,wfyN7kWM8UKgj:VsnBm`[FOqSqzkF-I' );
define( 'LOGGED_IN_SALT',   'T&f.=D]aX{En:l5(;D=Yq)IGie`,d`]C>*Y*x8Hj>CL%~;tjLBB vXct~pKuG8$s' );
define( 'NONCE_SALT',       ']wF}vM>3Ogz`o8A1y?RdEy9f7VkDHGpKnb?k;bfF|PP0+[Ge@^qRo1nMxl)Bhgio' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'multiQuiz_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
